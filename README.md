**Ze względu na prace przy zadaniu na nie swoim sprzęcie nie ryzykowałem instalacji czystego symfony bez paczek (problemy były przy instalacji przez composera np. toolbara)**

**aktualizacja /vendor**
composer install
composer update
composer clearcache
composer require symfony/apache-pack

**ENV**

**VH**
<VirtualHost *:80>
	DocumentRoot "/var/www/html/test/public"
	ServerName test-local.pl
	DirectoryIndex index.php
	
	<Directory "/var/www/html/test/public">
	Options All
	AllowOverride All
	Require all granted
	</Directory>
</VirtualHost>


**PHP**
7.4.28

**PSQL**
psql 14.2


**tipy**

bin/console doctrine:schema:drop --force && bin/console doctrine:schema:update --force && bin/console doctrine:fixtures:load -n