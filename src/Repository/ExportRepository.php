<?php

namespace App\Repository;

use App\Entity\Export;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function PHPUnit\Framework\isNull;

/**
 * @method Export|null find($id, $lockMode = null, $lockVersion = null)
 * @method Export|null findOneBy(array $criteria, array $orderBy = null)
 * @method Export[]    findAll()
 * @method Export[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Export::class);
    }

    // /**
    //  * @return Export[] Returns an array of Export objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    */

    public function filter($arr)
    {
        $resp = $this->createQueryBuilder('e');
        if(is_array($arr))
        {
            foreach ($arr as $key=>$value)
            {
                if($key == 'timestamp') {
                    if($value['from'] != "")
                    {
                        $resp->andWhere('e.timestamp >= :from')
                            ->setParameter('from',new \DateTime($value['from'].' 00:00:00'));
                    }
                    if($value['to'] != "")
                    {
                        $resp->andWhere('e.timestamp <= :to')
                            ->setParameter('to',new \DateTime($value['to'].' 23:59:59'));
                    }
                }
                else {
                    if($value != 0) {
                        $resp->andWhere('e.'.$key.' = :val')
                            ->setParameter('val',$value);
                    }

                }
            }
        }

        return $resp->getQuery()
            ->getResult();
    }

    public function example()
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.institution_id = :val')
            ->setParameter('val', 1)
//            ->orderBy('e.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Export
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
