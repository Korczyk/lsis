<?php

namespace App\Controller;

use App\Entity\Export;
use App\Entity\Institution;
use App\Entity\User;
use App\Repository\ExportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ExportController extends AbstractController
{
    public function __construct(ExportRepository $repository, EntityManagerInterface $entityManager)
    {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
    }


    /**
     * @Route("/export", name="export")
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $exports = $this->entityManager->getRepository(Export::class)->findAll();

        return $this->render('export/index.html.twig', [
            'exports'=>$exports
        ]);
    }


    /**
     * @return void
     * @Route("/filter",name="filter")
     */
    public function filters(Request $request)
    {
        $institutions = $this->entityManager->getRepository(Institution::class)->findAll();
        $filters = $request->request->get('filter');

//        dd($filters);

        return $this->render('export/index.html.twig', [
            'exports'=>$this->repository->filter($filters),
            'institutions'=>$institutions,
        ]);
    }
}
