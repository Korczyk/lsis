<?php

namespace App\DataFixtures;

use App\Entity\Institution;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class InstitutionFixtures extends Fixture
{
    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        for ($i=0;$i<5;$i++)
        {
            $institution = new Institution();
            $institution->setName($this->faker->company);
            $manager->persist($institution);
        }
        $manager->flush();
    }
}
