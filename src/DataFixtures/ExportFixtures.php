<?php

namespace App\DataFixtures;

use App\Entity\Export;
use App\Entity\Institution;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ExportFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function getDependencies()
    {
        return [UserFixtures::class,InstitutionFixtures::class];
    }

    public function load(ObjectManager $manager): void
    {
        $users = $manager->getRepository(User::class)->findAll();
        $institutions = $manager->getRepository(Institution::class)->findAll();

        for($i=0;$i<20;$i++)
        {
            $export = new Export();
            $export->setName('Export'.$i)
                ->setTimestamp($this->faker->dateTime)
                ->setInstitutionId($institutions[rand(0,count($institutions)-1)])
                ->setUserId($users[rand(0,count($users)-1)]);
            $manager->persist($export);
        }
        $manager->flush();
    }
}
